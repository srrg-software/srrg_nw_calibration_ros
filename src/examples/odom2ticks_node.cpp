#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/JointState.h>

#include "kinematics/differential_drive.hpp"
#include "../utils/utils.h"

#include <list>

using namespace new_world_calibration;

DifferentialDrive<float>* robot;
sensor_msgs::JointState *joint;
ros::Publisher *tick_pub;

Eigen::Isometry2f current_odom;
bool first_message;
Eigen::Vector3f params;

void printBanner(const char** banner) {
    const char** b = banner;
    while(*b) {
        std::cerr << *b << std::endl;
        b++;
    }
}


const char* banner[]={
  "\n\nUsage:  odom2ticks_node -joint-topic <joint_topic> -odom-topic <odom_topic> -joint-state-topic <joint_state_topic> -params <kl kr baseline> \n",
  "Example:  odom2ticks_node -joint-topic /joint_state -odom-topic /odom -joint-state-topic /joint_state -params 0.1 0.1 0.4 \n\n",
  "Options:\n",
  "------------------------------------------\n",
  "-joint-topic <string>       desired topic name of joints of the platform",
  "-odom-topic <string>        topic name that contains the platform odometry",
  "-params <float*>            values for kl kr and baseline",
  "-h                          this help\n",
  0
};


void odomCallback(const nav_msgs::Odometry& odom){

  float x = odom.pose.pose.position.x;
  float y = odom.pose.pose.position.y;
  float z = odom.pose.pose.position.z;
  float qx = odom.pose.pose.orientation.x;
  float qy = odom.pose.pose.orientation.y;
  float qz = odom.pose.pose.orientation.z;

  Vector6f new_pose;
  new_pose << x, y, z, qx, qy, qz;
  Eigen::Isometry3f new_T = v2t(new_pose);
  if(first_message) {
    current_odom = iso2(new_T);
    first_message = false;
    return;
  }
  
  Eigen::Isometry2f relative_2d_pose = current_odom.inverse() * iso2(new_T);

  Eigen::Vector2f predicted_ticks;  
  predicted_ticks.setZero();
  robot->predictTicks(predicted_ticks, relative_2d_pose, params);

  joint->header.stamp = odom.header.stamp;
  joint->header.seq++;
  joint->position[0] += predicted_ticks(0);
  joint->position[1] += predicted_ticks(1);
  
  std::cerr << ".";
  tick_pub->publish(*joint);
}

int main(int argc, char ** argv){

  if(argc < 2) {
    printBanner(banner);
    return 1;
  }

  ros::init(argc, argv, "odom2ticks_node");
  ros::NodeHandle nh("~");
  ROS_INFO("odom2ticks_node started....");
  
  std::string odom_topic = "/odom";
  std::string joint_topic = "/joint_state";

  params << 0.1, 0.1, 0.4;
    
  int c=1;
  std::string filename;

  while(c<argc){
    if (! strcmp(argv[c],"-h")){
      printBanner(banner);
      return 1;
    }
    else if(! strcmp(argv[c],"-joint-topic")){
      c++;
      joint_topic = argv[c];
    }
    else if(! strcmp(argv[c],"-odom-topic")){
      c++;
      odom_topic = argv[c];
    }
    else if(! strcmp(argv[c],"-params")){
      c++;
      params(0) = std::atof(argv[c]);
      c++;
      params(1) = std::atof(argv[c]);
      c++;
      params(2) = std::atof(argv[c]);
    }
    c++;
  }

  std::cerr<<"odom-topic:   "<<odom_topic<<std::endl;
  std::cerr<<"joint-topic: "<<joint_topic<<std::endl;
  std::cerr<<"params:      ";
  for(int i=0; i < 3; ++i)
    std::cerr<<params(i)<<" ";
  std::cerr << std::endl;

  ros::Subscriber odom_sub = nh.subscribe(odom_topic, 1000, odomCallback);
  tick_pub = new ros::Publisher();
  *tick_pub = nh.advertise<sensor_msgs::JointState>(joint_topic, 1000); 

  joint = new sensor_msgs::JointState();
  joint->header.seq = 0;
  joint->header.frame_id = "fake_joints";
  joint->name.push_back("joint_left");
  joint->name.push_back("joint_right");
  joint->position.push_back(0.f); 
  joint->position.push_back(0.f);
  joint->velocity.push_back(0.f);
  joint->velocity.push_back(0.f);
  joint->effort.push_back(0.f);
  joint->effort.push_back(0.f);

  robot = new DifferentialDrive<float>();
  
  first_message = true;
  current_odom.setIdentity();

  ros::spin();

  return 0;
}  

